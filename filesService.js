const fs = require('fs');
const path = require('path');
const url = require('url');

const directoryPath = path.join(__dirname, 'files');

function getPermitExt(name) {
  const permitExt = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];
  return permitExt.includes(path.extname(name).slice(1));
}

function createFile (req, res, next) {
  if(Object.keys(req.body).length===0 || !req.body.filename) {
    res.status(400).send({ "message": "Please specify right parameters" });
  } else {
    const filename = req.body.filename.toString();
    fs.readdir(directoryPath, function (err, files) {
      if (err) {
        res.status(400).send({ "message": "Path error" });
      }
      if(files.includes(filename) || !req.body.content || !getPermitExt(filename)) {
        res.status(400).send({ "message": "Please specify right parameters" });
      } else {
        const content = req.body.content;
        fs.appendFile(path.join(__dirname, 'files', filename), content, function(err) {
          if(err) throw err;
          res.status(200).send({ "message": "File created successfully" });
        });
      }
    });
  }
}

function getFiles (req, res, next) {

  fs.readdir(directoryPath, function (err, files) {
    if (err) {
      res.status(400).send({
        "message": "Client error"
      });
    }
    if(!files.length) {
      res.status(400).send({
        "message": "Client error"
      });
    } else {
      const loadedFiles = [];
      files.forEach(function (file) {
        loadedFiles.push(file);
      });
      res.status(200).send({
        "message": "Success",
        "files": loadedFiles});
    }
  });
}

const getFile = (req, res, next) => {
  const filePath = url.parse(req.url, true).pathname;
  if (!fs.existsSync(directoryPath+filePath)) {
    res.status(400).send({
      "message": "Not found file with required filename"
    });
  } else {
    fs.readFile(directoryPath+filePath, 'utf-8', (err, data) => {
      if(err) {
        console.log('Read file error');
      }
      const uploadedDate = fs.statSync(directoryPath+filePath).birthtime;
      res.status(200).send({
        "message": "Success",
        "filename": path.basename(filePath),
        "content": data,
        "extension": path.extname(filePath).slice(1),
        "uploadedDate": uploadedDate});
    })
  }
}

const writeFile = (req, res, next) => {
  if (!fs.existsSync(directoryPath+req.url)) {
    res.status(400).send({
      "message": "Not found file with required filename"
    });
  } else if(!req.body.content) {
    res.status(400).send({
      "message": "Please specify right content parameter"
    });
  } else {
    fs.writeFile(directoryPath+req.url, req.body.content, (err, data) => {
      if(err) {
        console.log('Write file error');
      }
      res.status(200).send({
        "message": "Success",
        "filename": path.basename(req.url)});
    })
  }
}

const deleteFile = (req, res, next) => {
  fs.unlink(directoryPath+req.url, (err) => {
    if (err && err.code === "ENOENT") {
      res.status(400).send({
        "message": "Error! File doesn't exist"
      });
    } else if (err) {
      res.status(400).send({
        "message": "Something went wrong. Please try again later"
      });
    } else {
      res.status(200).send({
        "message": `Successfully removed file: ${path.basename(req.url)}`
      });
    }
  });
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  writeFile,
  deleteFile
}
